# Map Viewer

Kenabil's Map Viewer used on the Awakening server.

The original repository for this map viewer no longer exists. This repository contains the source code and all the releases that existed a few weeks before it was deleted.

[See reference for details.](https://wiki.wurm-unlimited.net/page/Kenabil%27s_Map_Viewer)
